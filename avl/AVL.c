#include <stdio.h>
#include <stdlib.h>


typedef struct nodo {
  char dado;
  int fatbal;
  struct nodo *esq, *dir;
} arvoreAVL;

void emOrdem(arvoreAVL **eainicio) {
  if (*eainicio == NULL)
    printf (" ");
  else {
    emOrdem (&((*eainicio)->esq));
    printf ("%c " , (*eainicio)->dado);
    emOrdem (&((*eainicio)->dir));
  }
}
void preOrdem(arvoreAVL **eainicio) {
  if (*eainicio == NULL)
    printf (" ");
  else {
    printf ("%c " , (*eainicio)->dado);
    preOrdem (&((*eainicio)->esq));
    preOrdem (&((*eainicio)->dir));
  }
}
void posOrdem(arvoreAVL **eainicio) {
  if (*eainicio == NULL)
    printf (" ");
  else {
    posOrdem (&((*eainicio)->esq));
    posOrdem (&((*eainicio)->dir));
    printf ("%c " , (*eainicio)->dado);
  }
}
int BuscaAVL(arvoreAVL *adesc, char dadobusca) {

  arvoreAVL *paux, *pant, *pP, *pQ, *pantP, *pnovo;
  int poschave;
  int achou;

  if (adesc == NULL)
    printf("Arvore vazia!");
  else{
     paux = adesc->dir;
     pP = paux;
     pant = adesc;
     pantP = adesc;
     achou = 0;
     while ((!achou) && (paux != NULL)) {
        if (paux->fatbal != 0) {
        pP = paux;
        pantP = pant;
     }
     pant = paux;
     if (dadobusca == paux->dado)
        achou = 1;
     else
        if (dadobusca < paux->dado)
           paux = paux->esq;
        else
           paux = paux->dir;
     }
  }
  if (achou)
     printf("Elemento encontrado na arvore.\n");
  else
      printf("elemento nao existe!");

/*  else {
    if((*eainicio)->dado == dadobusca)
       return 1;
    else if((*eainicio)->dado > dadobusca){
       BuscaAVL(&((*eainicio)->dir), dadobusca);
       return 1;
    }
    else if((*eainicio)->dado < dadobusca){
       BuscaAVL(&((*eainicio)->esq), dadobusca);
       return 1;
    }
    else{
       return 0;
    }
  }*/
}

void LeArv(arvoreAVL **eainicio) {
  if (*eainicio == NULL)
    printf (".");
  else {
    printf ("%c" , (*eainicio)->dado);
    LeArv (&((*eainicio)->esq));
    LeArv (&((*eainicio)->dir));
  }
}

void InicializaAVL (arvoreAVL **eainicio){
  *eainicio = malloc (sizeof (arvoreAVL));
  (*eainicio)->esq = NULL;
  (*eainicio)->dir = NULL;
  }

void InsereAVL (arvoreAVL *adesc, char dadonovo) {
  arvoreAVL *paux, *pant, *pP, *pQ, *pantP, *pnovo;
  int poschave;
  int achou;

void RotacaoSimples (){
  if (pP->fatbal == 1) {
    pP->dir = pQ->esq;
    pQ->esq = pP;
  }
  else {
    pP->esq = pQ->dir;
    pQ->dir = pP;
  }
  paux = pQ;
  pP->fatbal = 0;
  pQ->fatbal = 0;
}


void RotacaoDupla (){
  if (pP->fatbal == 1) {
    paux = pQ->esq;
    pQ->esq = paux->dir;
    paux->dir = pQ;
    pP->dir = paux->esq;
    paux->esq = pP;
  }
  else {
    paux = pQ->dir;
    pQ->dir = paux->esq;
    paux->esq = pQ;
    pP->esq = paux->dir;
    paux->dir = pP;
  }
  if (paux->fatbal == -poschave){
    pP->fatbal = 0;
    pQ->fatbal = poschave;
  }
  else
    if (paux->fatbal == 0) {
      pP->fatbal = 0;
      pQ->fatbal = 0;
    }
    else {
      pP->fatbal = -poschave;
      pQ->fatbal = 0;
    }
  paux->fatbal = 0;
}

void AjustaFatoresAVL (){
  if (dadonovo < pP->dado){
    pQ = pP->esq;
    paux = pP->esq;
  }
  else {
    pQ = pP->dir;
    paux = pP->dir;
  }
  while (paux->dado != dadonovo)
    if (dadonovo < paux->dado) {
      paux->fatbal = paux->fatbal - 1;
      paux = paux->esq;
    }
    else {
      paux->fatbal = paux->fatbal + 1;
      paux = paux->dir;
    }
}

void BalanceiaAVL (){
  if (dadonovo < pP->dado)
    poschave = -1;
  else
    poschave = 1;
  if (pP->fatbal == 0)
    pP->fatbal = poschave;
  else
    if (pP->fatbal == -poschave)
      pP->fatbal = 0;
    else {
      if (pQ->fatbal * poschave > 0)
        RotacaoSimples ();
      else
        RotacaoDupla ();
      if (pantP->dir == pP)
        pantP->dir = paux;
      else
        pantP->esq = paux;
    }
}

  paux = adesc->dir;
  pP = paux;
  pant = adesc;
  pantP = adesc;
  achou = 0;
  while ((!achou) && (paux != NULL)) {
    if (paux->fatbal != 0) {
      pP = paux;
      pantP = pant;
    }
    pant = paux;
    if (dadonovo == paux->dado)
      achou = 1;
    else
      if (dadonovo < paux->dado)
        paux = paux->esq;
      else
        paux = paux->dir;
  }
  if (achou)
    printf("este dado ja esta presente na arvore\n");
  else {
    pnovo = malloc (sizeof (arvoreAVL));
    pnovo->dado = dadonovo;
    pnovo->esq = NULL;
    pnovo->dir = NULL;
    pnovo->fatbal = 0;
    if (adesc->dir == NULL)
      adesc->dir = pnovo;
    else {
      if (dadonovo < pant->dado)
        pant->esq = pnovo;
      else
        pant->dir = pnovo;
      AjustaFatoresAVL ();
      BalanceiaAVL ();
    }
  }
}

int main ()
{
  arvoreAVL *aini;
  char c, resp, dadobusca;
  int opc;
  InicializaAVL (&aini);

  printf("1- Inserir \n");
  printf("2- Buscar \n");
  printf("3- Imprimir \n");
  printf("0- Sair \n");
  printf("?:");
  scanf("%d", &opc);

while(opc!=0){

   switch(opc){
      case 1:
        printf ("entre com valor a ser inserido na arvore\n");
        fflush(stdin);
        scanf ("%c" , &c);
        InsereAVL (aini, c);
        printf("\n\n");
           break;
      case 2:
        printf("\n\n");
        printf("Valor para Busca:");
        fflush(stdin);
        scanf("%c", &dadobusca);
        printf("\n");
        BuscaAVL(aini, dadobusca);
        printf("\n\n");
           break;
      case 3:
        printf("\n\n");
        printf("Em ordem:\n");
        emOrdem (&((aini)->dir));
        printf("\nPre ordem:\n");
        preOrdem (&((aini)->dir));
        printf("\nPos ordem:\n");
        posOrdem (&((aini)->dir));
        printf("\n\n");
        printf("\nArvore:\n");
        LeArv (&((aini)->dir));
        printf("\n\n");
           break;
      case 0:
        system("pause");
        return 0;
           break;
   }
   printf("1- Inserir \n");
   printf("2- Buscar \n");
   printf("3- Imprimir \n");
   printf("0- Sair \n");
   printf("?:");
   scanf("%d", &opc);

}
  printf("\n\n");
  system("pause");
}
