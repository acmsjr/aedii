#include <stdlib.h>
#include <fstream>
#include <string>
#include <sstream>
#include <queue>
#include <iostream>
using namespace std;

class NoDaArvore  
      {
      private:
          bool ehFolha;                // indica se o no � uma folha   
          int totalDeChaves;           // total de chaves presentes no no
          int ordem;                   // ordem do no
          int *chaves;                 // lista com as chaves do no
          NoDaArvore* *filhos;         // lista com os filhos do no 
          NoDaArvore* ascendente;      // no ascendente 
          NoDaArvore* irmaoAEsquerda;  // no irm�o � esquerda
          NoDaArvore* irmaoADireita;   // no irm�o � direita      
      public:

     bool cria(int ordemValue, bool ehfolha)
       {  
          ehFolha = ehfolha;
          totalDeChaves = 0; 
          ordem = ordemValue;        
          ascendente = NULL;
          irmaoAEsquerda = NULL;
          irmaoADireita = NULL;

          chaves = (int *) malloc((ordem) * sizeof(int)); 
          filhos = ehfolha ? NULL : (NoDaArvore**) malloc((ordem + 1) * sizeof(NoDaArvore*)); 
          
          if(chaves == NULL || (!ehfolha && filhos == NULL))
              return false;        
          
          if(filhos != NULL)
              for(int i=0; i < ordem + 1; i++)
                  filhos[i] = NULL;   
          
          return true;               
       }
       
//---------------------------------------------------------------------------------------------------------
     void destroi()
       {  
           ascendente = NULL;
           irmaoAEsquerda = NULL;
           irmaoADireita = NULL;
                 
           if(!ehFolha)      

              for(int i = 0; i < totalDeChaves + 1; i++)
                free(filhos[i]);	

          free(filhos);
          free(chaves);        
       }  
//---------------------------------------------------------------------------------------------------------

     bool ehfolha(){
         return ehFolha;     
     }       
//---------------------------------------------------------------------------------------------------------           

     int getNumeroDeChaves()
     {
        return totalDeChaves;
     }     
//---------------------------------------------------------------------------------------------------------

     NoDaArvore* getAscendente(){
         return ascendente;     
     }       
//---------------------------------------------------------------------------------------------------------

     NoDaArvore* getIrmaoAEsquerda(){
         return irmaoAEsquerda;     
     }             
//---------------------------------------------------------------------------------------------------------

     NoDaArvore* getIrmaoADireita(){
         return irmaoADireita;     
     }
//---------------------------------------------------------------------------------------------------------

     void setAscendente(NoDaArvore* as){
         ascendente = as;     
     }     
//---------------------------------------------------------------------------------------------------------

     void setIrmaoAEsquerda(NoDaArvore* iE){
         irmaoAEsquerda = iE;     
     }     
//---------------------------------------------------------------------------------------------------------

     void setIrmaoADireita(NoDaArvore* iD){
         irmaoADireita = iD; 
     }      
// --------------------------------------------------------------------------------------------------------

     NoDaArvore* getFilhoDaPosicao(int posicao){

         if(!ehFolha && posicao >= 0 && posicao < totalDeChaves + 1)
             return filhos[posicao];
             
         return NULL;
         
     }
//---------------------------------------------------------------------------------------------------------

     int getChaveDaPosicao(int posicao){
         
         if(posicao >= 0 && posicao < totalDeChaves)
             return chaves[posicao];
             
         return -1;
         
     } 
//-----------------------------------------------------------------------------------------------------------

     int getPosicaoDaChave(int chave){
            
        for(int i = 0; i < totalDeChaves; i++){
           if(chave == chaves[i])
               return i;
        }
        
        return -1;
         
     }  
//-----------------------------------------------------------------------------------------------------------

     int getPosicaoDoFilho(NoDaArvore* no){
        
        if(!ehFolha){
            for(int i = 0; i < totalDeChaves + 1; i++){
              if(filhos[i] == no)
                   return i;
            }
		}
        
        return -1;
         
     }  
//---------------------------------------------------------------------------------------------------------

     void adicionaChaveNaPosicao(int chave, int posicao){

        if(posicao >= 0 && posicao < ordem){  

            totalDeChaves++;

            for(int i = (totalDeChaves - 1); i > posicao; i--)
               chaves[i] = chaves[i - 1];
              
            chaves[posicao] = chave;   
            
        }   
        
     }
//---------------------------------------------------------------------------------------------------------     

     int adicionaChave(int chave){

        int posicao = getPosicaoParaANovaChave(chave); 
        
        adicionaChaveNaPosicao(chave, posicao);
        
        return posicao;
         
     } 
//---------------------------------------------------------------------------------------------------------     

     void substitueChaveNaPosicao(int chave, int posicao){
         
         if(posicao >= 0 && posicao < totalDeChaves)

             chaves[posicao] = chave;
              
     }
//---------------------------------------------------------------------------------------------------------

     void adicionaFilho(NoDaArvore* no, int posicao){

        if(filhos == NULL || no == NULL || posicao < 0 || posicao > ordem)
            return; 

        NoDaArvore* p = posicao > 0 ? filhos[posicao - 1] : NULL; 
        
        no->setIrmaoAEsquerda(p); 
        
        if(p != NULL)
            p->setIrmaoADireita(no);  
        
        p = posicao < totalDeChaves + 1 ? filhos[posicao] : NULL; 
        
        no->setIrmaoADireita(p); 
        
        if(p != NULL)
            p->setIrmaoAEsquerda(no);   
        
        
        p = NULL;

        for(int i = totalDeChaves; i > posicao; i--)
           filhos[i] = filhos[i - 1]; 

        filhos[posicao] = no;     
     } 
//--------------------------------------------------------------------------------------------------------- 

     int* divideChaves()
     {  
        int teto = (int) ordem / 2;

        if(ordem % 2 != 0)
            teto++;
        
        int *chavesSeparadas;
        chavesSeparadas = (int *) malloc((totalDeChaves - teto) * sizeof(int));
        
        for(int i = teto; i < totalDeChaves; i++){
           chavesSeparadas[i - teto] = chaves[i];
           chaves[i] = 0;
        }
        
        totalDeChaves = teto;
        
        return chavesSeparadas;
     }  
//--------------------------------------------------------------------------------------------------------- 

     NoDaArvore** divideFilhos()
     {  

        if(ehFolha)
            return NULL;        
                        
        int teto = (int) ordem / 2;

        if(ordem % 2 != 0)
            teto++;
        
        NoDaArvore* *filhosSeparados;
        filhosSeparados = (NoDaArvore **) malloc((totalDeChaves + 1 - teto) * sizeof(NoDaArvore*));

        filhos[teto]->setIrmaoAEsquerda(NULL);

        for(int i = teto; i < totalDeChaves + 1; i++){
           filhosSeparados[i - teto] = filhos[i];
		   filhosSeparados[i - teto]->setAscendente(NULL);
           filhos[i] = NULL; 
        }

        filhos[teto - 1]->setIrmaoADireita(NULL);
        
        return filhosSeparados;
     }  
//---------------------------------------------------------------------------------------------------------

     NoDaArvore* retiraUltimoFilho()
     {                  
        return retiraFilhoDaPosicao(totalDeChaves);
     }
//---------------------------------------------------------------------------------------------------------           

     NoDaArvore* retiraFilhoDaPosicao(int posicao)
     {  

        if(!ehFolha && posicao >= 0 && posicao < totalDeChaves + 1){ 
                           
            NoDaArvore* p = filhos[posicao];
            
			if(posicao > 0)
				filhos[posicao - 1]->setIrmaoADireita(p->getIrmaoADireita());
			
			if(posicao < totalDeChaves)
				filhos[posicao + 1]->setIrmaoAEsquerda(p->getIrmaoAEsquerda());

            p->setAscendente(NULL);
            p->setIrmaoAEsquerda(NULL);
            p->setIrmaoADireita(NULL);

            for(int i = posicao; i < totalDeChaves; i++)
                filhos[i] = filhos[i + 1];

            filhos[totalDeChaves] = NULL;

            return p;
        }
        
        return NULL;
     }     
//---------------------------------------------------------------------------------------------------------           

     NoDaArvore* retiraPrimeiroFilho()
     {          
        return retiraFilhoDaPosicao(0);
     }      
//---------------------------------------------------------------------------------------------------------           

     int retiraUltimaChave()
     {                  
        return retiraChaveDaPosicao(totalDeChaves - 1);
     }
//---------------------------------------------------------------------------------------------------------           

     int retiraChaveDaPosicao(int posicao)
     {  
        if(posicao >= 0 && posicao < totalDeChaves){   
		
            int chave = chaves[posicao];
            
            for(int i = posicao; i < totalDeChaves - 1; i++)
                chaves[i] = chaves[i + 1];
            
            chaves[totalDeChaves - 1] = 0;
            
            totalDeChaves--;
            
            return chave;
        }
        
        return -1;
     }     
//---------------------------------------------------------------------------------------------------------           

     int retiraPrimeiraChave()
     {          
        return retiraChaveDaPosicao(0);
     }
//--------------------------------------------------------------------------------------------------------- 

     void removeFilhoDaPosicao(int posicao){
        NoDaArvore* p = retiraFilhoDaPosicao(posicao);     
        
		if(p != NULL)
			free(p);   
     }
//---------------------------------------------------------------------------------------------------------

     void removeChaveDaPosicao(int posicao){
        
        int chave = retiraChaveDaPosicao(posicao);
        
        chave = 0;
     }
//---------------------------------------------------------------------------------------------------------  

     int removeChave(int chave){
        
        int posicao = getPosicaoDaChave(chave); 
        
        removeChaveDaPosicao(posicao);
        
        return posicao;
         
     }  

     int getPosicaoParaANovaChave(int chave)
     {  
        int pos = 0;
        
        for(int i = 0; i < totalDeChaves; i++){
           if(chave < chaves[i])
               break;
           else
               pos = i + 1;
        }
        
        return pos;
     }     
//---------------------------------------------------------------------------------------------------------

     bool contemFilho(NoDaArvore* no){

         if(ehFolha || no == NULL)
             return false;

         for(int i = 0; i < totalDeChaves + 1; i++)
             if(filhos[i] == no)
                 return true;
         
         return false;           
     }
//---------------------------------------------------------------------------------------------------------

     bool contemChave(int chave){
         
         for(int i = 0; i < totalDeChaves; i++)
             if(chaves[i] == chave)
                 return true;
         
         return false;            
     }    
//---------------------------------------------------------------------------------------------------------

     NoDaArvore* getProximoNoParaAChave(int chave){
         
         if(ehFolha)
             return NULL;
         
         for(int i = 0; i < totalDeChaves; i++){
             if(chave < chaves[i])
                 return filhos[i];   
                         
         }       
         
         return filhos[totalDeChaves];   
     } 
//---------------------------------------------------------------------------------------------------------
     };

//---------------------------------------------------------------------------------------------------------------    
     class ArvoreB
      {
      private:
              NoDaArvore* raiz;          
              int ordem;
              int totalChaves;
//---------------------------------------------------------------------------------------------------------------

     NoDaArvore* getIrmaoComMaisChavesQueOMinimo(NoDaArvore* no, int teto){
         
         NoDaArvore* irmao = no->getIrmaoAEsquerda();

         if(irmao != NULL && irmao->getNumeroDeChaves() > teto - 1)
             return irmao;

         irmao = no->getIrmaoADireita();
         
         if(irmao != NULL && irmao->getNumeroDeChaves() > teto - 1)
             return irmao;
         
         irmao = NULL;

         return NULL;  
                
     }
//---------------------------------------------------------------------------------------------------------------

     NoDaArvore* getFilhoComMaisChavesQueOMinimo(int chave, NoDaArvore* no, int teto, int *nivelAbaixoDoNo){
         
         *nivelAbaixoDoNo = 1;
         int nivelEsq = 0;

         NoDaArvore* filho = getFilhoAEsquerda(chave, no);
         NoDaArvore* filhoMaisADireita = getFilhoMaisADireita(filho, &nivelEsq);   
         
         *nivelAbaixoDoNo += nivelEsq;

         if(filhoMaisADireita != NULL && filhoMaisADireita->getNumeroDeChaves() > teto - 1)            
             return filhoMaisADireita;

         int nivelDir = 0;
         filho = getFilhoADireita(chave, no);
         NoDaArvore* filhoMaisAEsquerda = getFilhoMaisAEsquerda(filho, &nivelDir);
         
         if(filhoMaisAEsquerda != NULL && filhoMaisAEsquerda->getNumeroDeChaves() > teto - 1){
             *nivelAbaixoDoNo += nivelDir;              
             return filhoMaisAEsquerda;
		  }

         return filhoMaisADireita;  
                  
     }
//-----------------------------------------------------------------------------------------------------------------------

     NoDaArvore* getFilhoExtremo(int chave, NoDaArvore* no, bool ehODaDireita, int *nivel){
         
         NoDaArvore* noAux = ehODaDireita ? getFilhoADireita(chave, no) : getFilhoAEsquerda(chave, no);
         NoDaArvore* n = no;
         
         *nivel = 0;
         
         while(noAux != NULL){
            *nivel += 1;            
            n = noAux;
            int posicao = ehODaDireita ? noAux->getNumeroDeChaves() - 1 : 0;
            int k = noAux->getChaveDaPosicao(posicao); 
            noAux = ehODaDireita ? getFilhoADireita(k, noAux) : getFilhoAEsquerda(k, noAux);               
         }    
         
         return n;             
     } 
//----------------------------------------------------------------------------------------------------------

     NoDaArvore* getFilhoMaisAEsquerda(NoDaArvore* no, int *nivel){
         if(no == NULL)
             return NULL;
             
         if(no->ehfolha())
             return no;    
         
         int chave = no->getChaveDaPosicao(0);
             
         return getFilhoExtremo(chave, no, false, nivel);     
     }
//-----------------------------------------------------------------------------------------------------------  

      NoDaArvore* getFilhoMaisADireita(NoDaArvore* no, int *nivel){
         if(no == NULL)
             return NULL;
             
         if(no->ehfolha())
             return no;    
         
         int chave = no->getChaveDaPosicao(no->getNumeroDeChaves() - 1);
         
         return getFilhoExtremo(chave, no, true, nivel);     
     }            
//----------------------------------------------------------------------------------------------------------

     NoDaArvore* getFilhoAEsquerda(int chave, NoDaArvore* no){
         
         return getFilho(chave, no, false);     
     }
//-----------------------------------------------------------------------------------------------------------  

      NoDaArvore* getFilhoADireita(int chave, NoDaArvore* no){
         
         return getFilho(chave, no, true);     
     }
// ------------------------------------------------------------------------------------------------------------     

     NoDaArvore* getFilho(int chave, NoDaArvore* no, bool ehODaDireita){
         
         int posicao = no != NULL ? no->getPosicaoDaChave(chave) : -1; 
         
         if(posicao < 0)
             return NULL;
             
         return no->getFilhoDaPosicao(ehODaDireita ? posicao + 1 : posicao);           
     }     
//---------------------------------------------------------------------------------------------------------------

     NoDaArvore* getNoAInserirNovaChave(int chave, int nivelDeParada, int* nivelDoNo){
          
          *nivelDoNo = 1;
          
          NoDaArvore* noAux = raiz;
          
          while(noAux != NULL){       
                             
             if((nivelDeParada == *nivelDoNo) || noAux->ehfolha())
                 return noAux;
                                                
             noAux = noAux->getProximoNoParaAChave(chave); 
			 
             *nivelDoNo = *nivelDoNo + 1;
                                            
          }   

		   return NULL; 
     }     
// --------------------------------------------------------------------------------------------------------------  

     void inserir(int chave, NoDaArvore* no, int nivelDoNo, NoDaArvore* filhoAEsquerda, NoDaArvore* filhoADireita)
     {    
          
          if(raiz == NULL){  

              raiz = (NoDaArvore*) malloc(sizeof(NoDaArvore)); 
              
              if(!raiz->cria(ordem, nivelDoNo == -1)){
                  cout << "\nMemoria insuficiente!" << endl;
                  cin.get();
                  return;
              }
              
              raiz->adicionaChave(chave);
              
              if(filhoAEsquerda != NULL && filhoADireita != NULL){                  
                  raiz->adicionaFilho(filhoAEsquerda, 0);
                  raiz->adicionaFilho(filhoADireita, 1);
                  
                  filhoAEsquerda->setAscendente(raiz);
                  filhoADireita->setAscendente(raiz);
              }
              
          }else{

                int posicao = no->adicionaChave(chave);
                
                if(filhoADireita != NULL){  

                   filhoADireita->setAscendente(no);
                   no->adicionaFilho(filhoADireita, posicao + 1);
                }

               if(no->getNumeroDeChaves() == ordem){

                   NoDaArvore **novosFilhos = no->divideFilhos();
                   int *novasChaves = no->divideChaves();
                   int newNumberOfKeys = ordem - no->getNumeroDeChaves();

                   NoDaArvore* novoNo;
                   
                   novoNo = (NoDaArvore*) malloc(sizeof(NoDaArvore)); 
                   
				   if(!novoNo->cria(ordem, no->ehfolha())){
					  cout << "\nMemoria insuficiente!" << endl;
					  cin.get();
					  return;
				   }

				   novoNo->setIrmaoADireita(no->getIrmaoADireita());
				   novoNo->setIrmaoAEsquerda(no);
				   
				   if(no->getIrmaoADireita() != NULL)
					  no->getIrmaoADireita()->setIrmaoAEsquerda(novoNo);
					  
				   no->setIrmaoADireita(novoNo);	

                   for(int i = 0; i < newNumberOfKeys; i++)
                       novoNo->adicionaChave(novasChaves[i]);
                   
				   free(novasChaves);

                   if(novosFilhos != NULL){
                                  
                       NoDaArvore* f = NULL;

                       for(int i=0; i < newNumberOfKeys + 1; i++){  
                           f = novosFilhos[i];        
                           f->setAscendente(novoNo);
                           novoNo->adicionaFilho(f, i);
						   novosFilhos[i] = NULL;
                       }
                         
                       f = NULL;

                       free(novosFilhos);
                   }

                   int chavePromovida = no->retiraUltimaChave();                       

                   filhoAEsquerda = no == raiz ? no : NULL; 
                   filhoADireita = novoNo;              
                   
                   if(no == raiz){
                       raiz = NULL; 
                       no = NULL;
                   }else
                       no = no->getAscendente();

                   inserir(chavePromovida, no, nivelDoNo - 1, filhoAEsquerda, filhoADireita);         
               } 
               
          }           
        
     } 
// --------------------------------------------------------------------------------------------------------------

    void redistribueChaves(NoDaArvore* no, NoDaArvore* ascendente, NoDaArvore* irmao, int chave, bool deslocaPonteiro){

        int chavePromovida = -1;
        int decrementoDePosicao = 0;

        int primeiraChave = no->getNumeroDeChaves() > 0 ? no->getChaveDaPosicao(0) : chave;
        
        int posNo = 0;
		        
        NoDaArvore* ponteiro = NULL;

        if(primeiraChave < irmao->getChaveDaPosicao(0)){

           if(deslocaPonteiro){
               ponteiro = irmao->retiraPrimeiroFilho();

               posNo = no->getNumeroDeChaves() + 1;    
           }
               
           chavePromovida = irmao->retiraPrimeiraChave();
           decrementoDePosicao++;
                                 
        }else{

            if(deslocaPonteiro) 
               ponteiro = irmao->retiraUltimoFilho();
			                  
           chavePromovida = irmao->retiraUltimaChave();  
        }

        int posicaoDaChavePromovida = ascendente->getPosicaoParaANovaChave(chavePromovida) - decrementoDePosicao;

        int chaveRebaixada = ascendente->getChaveDaPosicao(posicaoDaChavePromovida);

        ascendente->removeChaveDaPosicao(posicaoDaChavePromovida);
        ascendente->adicionaChave(chavePromovida);

        no->adicionaChave(chaveRebaixada);   

        if(ponteiro != NULL){

            ponteiro->setAscendente(no);
            no->adicionaFilho(ponteiro, posNo);  
        }         
    }
// --------------------------------------------------------------------------------------------------------------

    void fundeOsNos(NoDaArvore* no, NoDaArvore* ascendente, int chave, int nivelDoNo, int teto){

       NoDaArvore* irmao = no->getIrmaoADireita();

       int posNo = teto - 1;
       
       int totalNos = teto - 1;

       if(irmao == NULL)

           irmao = no;
       else

           totalNos = irmao->getNumeroDeChaves() + 1;

       int primeiraChave = irmao->getNumeroDeChaves() > 0 ? irmao->getChaveDaPosicao(0) : chave;

       int posicaoKeyRebaixada = ascendente->getPosicaoParaANovaChave(primeiraChave);
       if(posicaoKeyRebaixada > 0)

          posicaoKeyRebaixada--;

       int chaveRebaixada = ascendente->getChaveDaPosicao(posicaoKeyRebaixada);

       if(irmao == no){

           no = no->getIrmaoAEsquerda(); 

           posNo = no->getNumeroDeChaves() + 1; 
       }

       no->adicionaChave(chaveRebaixada);
          
       for(int i=0; i < irmao->getNumeroDeChaves(); i++)
           no->adicionaChave(irmao->getChaveDaPosicao(i));

       if(!irmao->ehfolha()){                                            

           NoDaArvore* f = NULL;

           for(int i=0; i < totalNos; i++){ 
                    
               f = irmao->getFilhoDaPosicao(i); 
                
               if(f != NULL){

                   f->setAscendente(no); 
                      
                   no->adicionaFilho(f, i + posNo);
               }
           } 
           
       }

       if(posicaoKeyRebaixada > 0 && ascendente->contemChave(primeiraChave))
          posicaoKeyRebaixada--;

       ascendente->removeFilhoDaPosicao(posicaoKeyRebaixada + 1);

       irmao = ascendente->getFilhoDaPosicao(posicaoKeyRebaixada + 1);
       
       no->setIrmaoADireita(irmao);
       
       if(irmao != NULL)
           irmao->setIrmaoAEsquerda(no);   

       if(ascendente == raiz && ascendente->getNumeroDeChaves() == 1){

           raiz = no;
           no->setAscendente(NULL);
           free(ascendente);
       }else

           remover(chaveRebaixada, ascendente, nivelDoNo - 1, nivelDoNo - 1);

    }   
// --------------------------------------------------------------------------------------------------------------

    void removePorCopia(NoDaArvore* no, int chave, int teto, int nivelDoNo){
        
        int nivelAbaixoDoNo = 0;

        NoDaArvore* filho = getFilhoComMaisChavesQueOMinimo(chave, no, teto, &nivelAbaixoDoNo);
        
        nivelDoNo += nivelAbaixoDoNo;
		

        int chavePromovida = -1;

        if(chave <= filho->getChaveDaPosicao(0)){

           chavePromovida = filho->getChaveDaPosicao(0);
                     
        }else

           chavePromovida = filho->getChaveDaPosicao(filho->getNumeroDeChaves() - 1);  

        no->substitueChaveNaPosicao(chavePromovida, no->getPosicaoDaChave(chave));

        remover(chavePromovida, filho,  nivelDoNo, -1); 
              
    }     
// --------------------------------------------------------------------------------------------------------------  

     void remover(int chave, NoDaArvore* no, int nivelDoNo, int nivelDeParada)
     {
                      
         int teto = (int) ordem / 2;
         if(ordem % 2 != 0)
            teto++;
         
         if(no->ehfolha() || nivelDoNo == nivelDeParada){

             NoDaArvore* ascendente = no->getAscendente();

             int posicao = no->removeChave(chave);

             if(no->getNumeroDeChaves() < teto - 1){

                NoDaArvore* irmao = getIrmaoComMaisChavesQueOMinimo(no, teto);
                
                if(irmao != NULL){

                    redistribueChaves(no, ascendente, irmao, chave, nivelDoNo == nivelDeParada);
                    
                }else if(ascendente != NULL){ 

                   fundeOsNos(no, ascendente, chave, nivelDoNo, teto);                    
                       
                }else if(no->getNumeroDeChaves() == 0)

                    raiz = NULL; 
                                       
             }
                                 
         }else{

            removePorCopia(no, chave, teto, nivelDoNo);   
         } 
        
     }         
// --------------------------------------------------------------------------------------------------------------

	void imprimeChaves(NoDaArvore* no){
		
			if(no == NULL)
				return;
			
			cout << "  [";
			
			for(int i = 0; i < no->getNumeroDeChaves(); i++)
				cout << "  " << no->getChaveDaPosicao(i);
			
			
			cout << "  ]";
			
	  }
 // -------------------------------------------------- PUBLIC ---------------------------------------------------             
      public:

     void cria(int m)
       { 
           ordem = m; 
           raiz = NULL;
           totalChaves = 0;        
        }
//--------------------------------------------------------------------------------------------------------------

     NoDaArvore* buscar(int chave)
     {
           int nivel;   
           return buscar(chave, &nivel);
     }     
//--------------------------------------------------------------------------------------------------------------

     NoDaArvore* buscar(int chave, int *nivelDoNo)
     {     
           *nivelDoNo = 1; 
           NoDaArvore* noAux = raiz;
           
           while(noAux != NULL){
                         
                if(noAux->contemChave(chave))
                   break;       
               
               noAux = noAux->getProximoNoParaAChave(chave);
			   
               *nivelDoNo = *nivelDoNo + 1;
               
           }
              
           return noAux;   
         
     }      
//--------------------------------------------------------------------------------------------------------------

     bool adicionaChave(int chave)
     {    
          if(ordem <= 2)
             return false;   
          
          int nivel = -1;
          
          NoDaArvore* no = raiz != NULL ? buscar(chave, &nivel) : NULL;

          if(no != NULL)
              return false;
              
          no = raiz != NULL ? getNoAInserirNovaChave(chave, -1, &nivel) : NULL;    
          
          totalChaves++;

          inserir(chave, no, nivel, NULL, NULL);
          
          return true;
     }
//--------------------------------------------------------------------------------------------------------------

     bool removeChave(int chave)
     {    
          
          if(raiz == NULL)
             return false;
          
          int nivel = 0;
		  
          NoDaArvore* no = buscar(chave, &nivel);
  
          if(no == NULL)
              return false;
  
          totalChaves--;

          remover(chave, no, nivel, -1);
          
          return true;
     }     
// -------------------------------------------------------------------------------------------------------------

    NoDaArvore* getRaiz(){
        return raiz;      
    } 
// -------------------------------------------------------------------------------------------------------------

    int getTotalChaves(){
        return totalChaves;    
    }    
// -------------------------------------------------------------------------------------------------------------

   int getAltura(){ 
       
       if(raiz != NULL){
                
          int altura = 1;
          
          if(!raiz->ehfolha()){ 
          
              NoDaArvore* no = getFilhoMaisAEsquerda(raiz, &altura);
              
              altura++;
              
          }
          
          return altura;
                
       } 
       
       return 0;   
   }    
// -------------------------------------------------------------------------------------------------------------

    int getOrdem(){
        return ordem;      
    }         
//--------------------------------------------------------------------------------------------------------------

    void imprime(int *altura, int *totalChaves){             
		 
		 *totalChaves = 0;
		 
		 if(raiz != NULL){
			 
			 NoDaArvore*  no = raiz;
			
			 *altura = 1;
		 
			 std::queue<NoDaArvore*> filaTemp; 
			 std::queue<NoDaArvore*> nos;

			 int* totalPorNivel;
			 
			 int nivelAtual = 1;
			 
			 totalPorNivel = (int*) malloc(sizeof(int));
			 
			 totalPorNivel[0] = 1;
			 
			 
			 *totalChaves = raiz->getNumeroDeChaves();
			 
			 
			 if(!no->ehfolha()){
				
				int contNos = 1, contNosProxNivel = 0;
				
				nivelAtual++;
				totalPorNivel = (int*) realloc(totalPorNivel, nivelAtual * sizeof(int));
				totalPorNivel[nivelAtual - 1] = 0;
			 
			 
				 while(no != NULL && !no->ehfolha()){
					
					if(contNos > totalPorNivel[nivelAtual - 2]){
						
						nivelAtual++;	
						contNos = 1;	
						totalPorNivel = (int*) realloc(totalPorNivel, nivelAtual * sizeof(int));
						
						totalPorNivel[nivelAtual - 1] = 0;
						
					}
					 	
					totalPorNivel[nivelAtual - 1] = totalPorNivel[nivelAtual - 1] + no->getNumeroDeChaves() + 1;
					
					for(int i = 0; i <= no->getNumeroDeChaves(); i++){  
						filaTemp.push(no->getFilhoDaPosicao(i));                 
						nos.push(no->getFilhoDaPosicao(i));
						*totalChaves = *totalChaves + no->getFilhoDaPosicao(i)->getNumeroDeChaves();
					}
					
					no = filaTemp.front();
					
					filaTemp.pop();
					
					contNos++;			
		 
				 }
				 
			 }

			 imprimeChaves(raiz);
			 
			 cout << endl;
			 
			 
			 *altura = nivelAtual;
			 
			 nivelAtual = 2;
			 
			 while(!nos.empty() && nivelAtual <= *altura){
				
				for(int k = 0; k < totalPorNivel[nivelAtual - 1]; k++){
					imprimeChaves(nos.front());
					nos.pop();
				}
				
				cout << endl;
				
				nivelAtual++;
			 } 
			 
			 free(totalPorNivel);
			 
		 }else
			*altura = 0;   			
         
    }
//--------------------------------------------------------------------------------------------------------------
    
     }; 
	      void imprimeArvore(ArvoreB* arvore){
         cout << "\nARVORE B DE ORDEM = " << arvore->getOrdem() << endl;
         
         cout << "\nALTURA DA ARVORE = " << arvore->getAltura() << "   TOTAL DE CHAVES = " << arvore->getTotalChaves() << "\n\n\n" << endl;
         
         cin.get();     
     }

     void imprimeArvoreCompleta(ArvoreB* arvore){
         cout << "\nARVORE B DE ORDEM = " << arvore->getOrdem() << endl;
         
         int altura = 0;
         
         int totalChaves = 0;
         
         arvore->imprime(&altura, &totalChaves);
		 
         cout << "\n\n\nALTURA DA ARVORE = " << altura << "   TOTAL DE CHAVES = " << totalChaves << "\n\n\n" << endl;;
         
         cin.get();         
     }

     bool carregaDadosDoArquivo(const char* caminho, ArvoreB* arvore, bool remover){
          
		  string linha;
		  
    	  int chave;
    	  
    	  ifstream arquivo (caminho); 
		  
    	  if (arquivo.is_open()){
		  
            int chaves = 0;                   
                               
    		while (!arquivo.eof()){
			
    		  getline(arquivo,linha);

    		  stringstream s(linha);
			  
    		  s >> chave;
    		  
    		  if(!arquivo.eof()){
			  
                  if(!remover){                            
					// inserir
                      if(!arvore->adicionaChave(chave)){
					  
                        if(arvore->getOrdem() <= 2){ 
						
            				cout << "Para ser uma Arvore B, a ordem deve ser > 2";
            				cin.get();
            				return false; 
							
                        }

                        chaves++;
        			  } 
                  }else{

                      if(arvore->removeChave(chave)){
                         chaves++;

                      }       
                  }
				  
              }
    		  
    		}
    		
    		if(remover)
    		    cout << "\nTOTAL DE CHAVES REMOVIDAS = " << chaves << "\n" << endl;
    		else if(chaves > 0)
    		    cout << "\nTOTAL DE CHAVES QUE NAO FORAM INSERIDAS POIS JA CONSTAVAM NA ARVORE = " << chaves << "\n" << endl;
    		    
    		arquivo.close();
			
    	  }
    
    	  else{ 
                cout << "Arquivo '" << caminho <<"' nao encontrado";
                cin.get();
                return false;
          }  
          
          return true;
     }

     bool carregaDadosDoArquivoInserir(const char* caminho, ArvoreB* arvore){
          return carregaDadosDoArquivo(caminho, arvore, false);
     }

     bool carregaDadosDoArquivoRemover(const char* caminho, ArvoreB* arvore){
          return carregaDadosDoArquivo(caminho, arvore, true);
     }

	 void removeChaveDaArvore(ArvoreB* arvore, int chave){
         cout << "\nREMOVENDO A CHAVE  " << chave << endl;
         arvore->removeChave(chave);
         cin.get();     
     }

     int main(){

         ArvoreB arvore;

         arvore.cria(3);
         
         const char* arquivoInserir = "exemploI.txt";
         const char* arquivoRemover = "exemploR.txt";
         
         cout << "\nINSERINDO CHAVES DO ARQUIVO '" << arquivoInserir << "'\n" << endl;

         if(!carregaDadosDoArquivoInserir(arquivoInserir, &arvore))
            return 1;         

         imprimeArvoreCompleta(&arvore);
         
         cout << "\nREMOVENDO CHAVES DO ARQUIVO '" << arquivoRemover << "'\n" << endl;

         if(!carregaDadosDoArquivoRemover(arquivoRemover, &arvore))
            return 1;   

         imprimeArvoreCompleta(&arvore);
		 
         
         return 0;    
     }     
